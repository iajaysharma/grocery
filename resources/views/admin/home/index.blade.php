@extends('admin.layouts.app')
@section('content')
<div class="row">
	<div class="col-md-6 col-lg-4"> 
		<div class="card card-hover">
			<div class="box bg-cyan text-center">
				<h1 class="font-light text-white">{{number_format($users,0)}}</h1>
				<h6 class="text-white">Users</h6>
			</div>
		</div> 
	</div>
	<div class="col-md-6 col-lg-4"> 
		<div class="card card-hover">
			<div class="box bg-cyan text-center">
				<h1 class="font-light text-white">{{number_format($categories,00)}}</h1>
				<h6 class="text-white">Categories</h6>
			</div>
		</div> 
	</div> 
</div>
@endsection