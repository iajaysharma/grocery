<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['namespace' => 'Api'],function($request){  
    Route::post('signin', 'UserController@signin'); 
    Route::post('forgot', 'UserController@forgot');  
    Route::post('signup', 'UserController@signup'); 
    Route::post('check-user', 'UserController@checkUser'); 
    Route::post('get-static-page', 'UserController@getStaticPage');  
    Route::post('get-app-setting', 'UserController@getAppSetting');  
    // Protected Routes

    Route::group(['middleware' => ['auth:sanctum','sanctum'],'prefix'=>'{prefix}'], function () {  
        Route::post('get-profile', 'UserController@getProfile'); 
        Route::post('logout', 'UserController@logout'); 
        Route::post('update-profile', 'UserController@updateProfile');
        Route::post('change-password', 'UserController@changePassword');
        Route::post('set-notification-status', 'UserController@setNotificationStatus');   
        Route::post('get-settings','UserController@getSettings');

        // User Only Routes
        Route::post('add-address','UserController@addAddress');  
        Route::get('get-user-addresses','UserController@getUserAddresses');  
        Route::post('delete-user-address','UserController@deleteUserAddress');


        // Store Only Routes
        Route::post('upload-store-image','StoreController@uploadStoreImage');
        Route::post('delete-store-image','StoreController@deleteStoreImage');
        Route::post('update-store-details','StoreController@updateStoreDetails');
        Route::get('get-store-details','StoreController@getStoreDetails');

        // Validate Prefix
        Route::group(['middleware' => ['validatePrefix']], function () {
             
        });


    });
    
});
 

Route::fallback(function(){
    return response()->json(['status'=>'false','message' => 'Page Not Found'], 404);
});
Route::fallback(function(){
    return response()->json(['status'=>'false','message' => 'Unauthorized','user_status'=>'false'], 401);
});