<?php

use Illuminate\Support\Facades\Route;
 
Route::get('/',function(){
  return redirect(route('admin.login'));
});
Route::get('admin',function(){
  return redirect(route('admin.login'));
});
Route::get('static/{slug}','Admin\PageController@view')->name('admin.pages.view');
Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>'admin.guest'], function () {
  Route::any('login','AuthController@login')->name('admin.login');  
  Route::post('forgot-password','AuthController@forgotPassword')->name('admin.forgotpassword');
  Route::any('reset-password/{token}','AuthController@resetPassword')->name('admin.resetpassword');
});
Route::group(['prefix' => 'admin','namespace'=>'Admin','middleware'=>'admin'], function () {
  Route::get('home','HomeController@index')->name('admin.home');
  Route::get('toggle-sidebar','HomeController@toggleSidebar')->name('admin.toggle-sidebar');
  Route::any('profile','HomeController@profile')->name('admin.profile');
  Route::any('change-password','HomeController@changePassword')->name('admin.changepassword');
  Route::get('logout','AuthController@logout')->name('admin.logout');

  // Settings Route
  Route::any('settings/add/{id?}','SettingController@add')->name('admin.settings.add');
  Route::any('settings/','SettingController@index')->name('admin.settings.index');
  Route::any('settings/datatables','SettingController@datatables')->name('admin.settings.datatables');

  // Pages Route
  Route::any('pages/add/{id?}','PageController@add')->name('admin.pages.add');
  Route::any('pages/','PageController@index')->name('admin.pages.index');
  Route::any('pages/datatables','PageController@datatables')->name('admin.pages.datatables');
  Route::any('pages/status','PageController@status')->name('admin.pages.status');

    // Categories Route
    Route::any('categories/add/{id?}','CategoryController@add')->name('admin.categories.add');
    Route::any('categories/','CategoryController@index')->name('admin.categories.index');
    Route::any('categories/datatables','CategoryController@datatables')->name('admin.categories.datatables');
    Route::any('categories/status','CategoryController@status')->name('admin.categories.status');
    Route::post('categories/delete/','CategoryController@delete')->name('admin.categories.delete');

    // Brands Route
    Route::any('brands/add/{id?}','BrandController@add')->name('admin.brands.add');
    Route::any('brands/','BrandController@index')->name('admin.brands.index');
    Route::any('brands/datatables','BrandController@datatables')->name('admin.brands.datatables');
    Route::any('brands/status','BrandController@status')->name('admin.brands.status');
    Route::post('brands/delete/','BrandController@delete')->name('admin.brands.delete');

    // Units Route
    Route::any('units/add/{id?}','UnitController@add')->name('admin.units.add');
    Route::any('units/','UnitController@index')->name('admin.units.index');
    Route::any('units/datatables','UnitController@datatables')->name('admin.units.datatables');
    Route::any('units/status','UnitController@status')->name('admin.units.status');
    Route::post('units/delete/','UnitController@delete')->name('admin.units.delete');
    
    // Email Templates Route
    Route::any('emailtemplates/add/{id?}','EmailTemplateController@add')->name('admin.emailtemplates.add');
    Route::any('emailtemplates/','EmailTemplateController@index')->name('admin.emailtemplates.index');
    Route::any('emailtemplates/datatables','EmailTemplateController@datatables')->name('admin.emailtemplates.datatables');
    Route::any('emailtemplates/status','EmailTemplateController@status')->name('admin.emailtemplates.status');

        // Faqs Route
    Route::any('faqs/add/{id?}','FaqController@add')->name('admin.faqs.add');
    Route::any('faqs/','FaqController@index')->name('admin.faqs.index');
    Route::any('faqs/datatables','FaqController@datatables')->name('admin.faqs.datatables');
    Route::any('faqs/status','FaqController@status')->name('admin.faqs.status');
    Route::post('faqs/delete/','FaqController@delete')->name('admin.faqs.delete');

    // Users Route
    Route::get('users','UserController@index')->name('admin.users.index');
    Route::any('users/datatables','UserController@datatables')->name('admin.users.datatables');
    Route::post('users/status','UserController@status')->name('admin.users.status');
    Route::get('users/view/{id}','UserController@view')->name('admin.users.view');
    Route::post('users/delete/','UserController@delete')->name('admin.users.delete');

    // Stores Routes
    Route::get('stores','StoreController@index')->name('admin.stores.index');
    Route::any('stores/datatables','StoreController@datatables')->name('admin.stores.datatables');
    Route::post('stores/status','StoreController@status')->name('admin.stores.status');
    Route::get('stores/view/{id}','StoreController@view')->name('admin.stores.view');
    Route::post('stores/delete/','StoreController@delete')->name('admin.stores.delete');

    Route::any('app-settings','UserController@appSetting')->name('app-settings');

    Route::get('custom-notification','SendNotificationController@index')->name('admin.notification.index');
    Route::any('custom-notification/datatables','SendNotificationController@datatables')->name('admin.notification.datatables');
    Route::any('send-custom-notification','SendNotificationController@send')->name('admin.send.notification');
    Route::post('custom-notification/delete/','SendNotificationController@delete')->name('admin.notification.delete');

});

