<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
class UserAddress extends Model
{  
	use SoftDeletes;   

    protected $fillable = [
        'user_id','address_type','address','latitude','longitude'
    ];  

    protected $casts = [
        'latitude' => 'double',
        'longitude' => 'double',
    ];

}
