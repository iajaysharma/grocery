<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
class Category extends Model
{  
	use SoftDeletes,Sluggable;   

    protected $fillable = [
        'title','slug','image','status','description','parent_id','position','icon'
    ]; 

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

}
