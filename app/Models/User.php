<?php

namespace App\Models;
 
use Illuminate\Foundation\Auth\User as Authenticatable; 
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
class User extends Authenticatable
{
    use HasApiTokens,SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password','profile_picture','country_code','mobile','token','status','notification','role','social_id','social_type','user_type','uuid'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'country_code'  => 'integer',
        'status'        => 'integer',
        'notification'  => 'integer',
    ];

    public static function getProfile($id){
        return User::where('id','=',$id)->first();
    }
}
