<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
class Brand extends Model
{  
	use SoftDeletes,Sluggable;   

    protected $fillable = [
        'name','slug','status','description','position','icon'
    ]; 

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}
