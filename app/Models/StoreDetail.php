<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDetail extends Model
{ 
    protected $fillable = [
        'store_id','store_name','address','contact_number'
    ];

    public function images(){
        return $this->hasMany(StoreImage::class,'store_id','store_id');
    }
}
