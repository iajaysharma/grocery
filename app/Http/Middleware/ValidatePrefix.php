<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
class ValidatePrefix
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {    
            $prefix = $request->route()->parameters()['prefix']; 
            $store = User::select('id')->where(['uuid'=>$prefix,'role'=>'STORE','status'=>1])->first();
            if(!$store){
                return response()->json(['status'=>'false','error'=>"Invalid Prefix",'user_status'=>'false']);
            }
            $request->current_store_id = $store->id;
        } catch (\Exception $e) {
            return response()->json(['status'=>'false','error'=>$e->getMessage(),'user_status'=>'false']);
        }
        return $next($request);
    }
}
