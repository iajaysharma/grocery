<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User; 
use App\Models\AppSetting; 
use Auth; 
use App\Lib\Helper;
use Validator;
use Session;
use Hash;
use DataTables;
use DB;
class StoreController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(){ 
        $title = "Stores";
        $breadcrumbs = [ 
            ['name'=>'Stores','relation'=>'Current','url'=>'']
        ];
        return view('admin/stores/index',compact('title','breadcrumbs'));
    }

    public function datatables()
    {
        $stores = User::where('role','STORE')->select(['id', 'name','country_code','mobile', 'email', 'status','created_at'])->get();

        return DataTables::of($stores)
            ->addColumn('action', function ($user) {
                return '<a target="_blank" href="'.route('admin.stores.view',$user->id).'" class="btn btn-xs btn-primary"><i class="fas fa-eye"></i> View</a>&nbsp;<a data-link="'.route('admin.stores.delete').'" id="delete_'.$user->id.'" onclick="confirm_delete('.$user->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
              
            ->editColumn('status',function($user){
                return Helper::getStatus($user->status,$user->id,route('admin.stores.status'));
            })   
            ->editColumn('created_at',function($user){
                return date("Y-m-d",strtotime($user->created_at));
            })
            ->editColumn('mobile',function($user){
                return "+".$user->country_code."-".$user->mobile;
            })    
            ->rawColumns(['status','action','name','mobile'])
            ->make(true);
    }

    public function status(Request $request)
    {
        $id = $request->id; 
        $row = User::whereId($id)->first();
        $row->status = $row->status=='1'?'0':'1';
        $row->save(); 
        return Helper::getStatus($row->status,$id,route('admin.stores.status')); 
    }
    
    public function view($id){
        $data = User::where('id',$id)->first(); 
        if($data){
            $title = "Profile - " .$data->first_name;
            $breadcrumbs = [ 
                ['name'=>"Stores",'relation'=>'link','url'=>route('admin.stores.index')],
                ['name'=>$title,'relation'=>'Current','url'=>'']
            ];
            return view('admin/stores/view',compact('title','breadcrumbs','data'));
        }else{
            return abort(404);
        }
    }

    public function delete(Request $request)
    {
        $user_id = $request->id;
        try{
            $delete = User::where('id','=',$user_id)->delete();   
            if($delete){ 
                return ["status"=>"true","message"=>"Record Deleted"]; 
            }else{
                return ["status"=>"error","message"=>"Could not deleted Record"]; 
            }
        }catch(\Exception $e){
            return ["status"=>"error","message"=>$e->getMessage()];   
        }
    } 

    
}
