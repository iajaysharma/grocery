<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
use Cache;
class BrandController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Brand";
            $breadcrumbs = [
                ['name'=>'Brand','relation'=>'link','url'=>route('admin.brands.index')],
                ['name'=>'Edit Brand','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Brand";
            $breadcrumbs = [
                ['name'=>'Brand','relation'=>'link','url'=>route('admin.brands.index')],
                ['name'=>'Add New Brand','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Brand::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [     
                    'name'              =>  'required|max:70',   
                    'position'          =>  'nullable|numeric',
                    'icon'              =>  'nullable|image', 
                    'image'             =>  'nullable|image',
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except(['icon']);  
                    if(empty($formData['position'])){
                        $formData['position'] = (int)Brand::count()+1;
                    } 
                    if($request->file('image')!==null){ 
                        $destinationPath = '/uploads/brands/';
                        $responseData = Uploader::doUpload($request->file('image'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['image'] = $responseData['file']; 
                        }                             
                    }
                    if($request->file('icon')!==null){ 
                        $destinationPath = '/uploads/brands/';
                        $responseData = Uploader::doUpload($request->file('icon'),$destinationPath); 
                        if($responseData['status']=="true"){
                            $formData['icon'] = $responseData['file']; 
                        }                             
                    } 
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Brand updated successfully.');
                    }else{  
                        Brand::create($formData);
                        Session::flash('success','Brand created successfully.');
                    }
                    Cache::forget('brands');
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/brands/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Brand";
        $breadcrumbs = [ 
            ['name'=>'Brand','relation'=>'Current','url'=>'']
        ]; 
        return view('admin/brands/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = Brand::select(['id', 'name','slug', 'status','icon','position']); 
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.brands.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.brands.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.brands.status'));
            }) 
            ->editColumn('image',function($page){
                return Helper::getImage($page->icon,70);
            }) 
            ->rawColumns(['status','action','image'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Brand::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.brands.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Brand::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
