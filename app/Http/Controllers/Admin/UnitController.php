<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Unit;
use App\Lib\Helper;
use App\Lib\Uploader;
use Session;
use DataTables;
use Validator; 
use Str; 
use Cache;
class UnitController extends Controller
{
    public function __construct()
    {
        
    }

    public function add(Request $request,$id=null){  
        if($id){
            $title = "Edit Unit";
            $breadcrumbs = [
                ['name'=>'Unit','relation'=>'link','url'=>route('admin.units.index')],
                ['name'=>'Edit Unit','relation'=>'Current','url'=>'']
            ];
        }else{
            $title = "Add New Unit";
            $breadcrumbs = [
                ['name'=>'Unit','relation'=>'link','url'=>route('admin.units.index')],
                ['name'=>'Add New Unit','relation'=>'Current','url'=>'']
            ];
        }
        $data = ($id)?Unit::find($id):array(); 
        if($request->ajax() && $request->isMethod('post')){
            try {
                $rules = [     
                    'name'              =>  'required|max:30',   
                    'position'          =>  'nullable|numeric', 
                ];
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()){
                    return response()->json(array('errors' => $validator->messages()), 422);
                }else{
                    $formData = $request->except(['icon']);  
                    if(empty($formData['position'])){
                        $formData['position'] = (int)Unit::count()+1;
                    }  
                    if($id){    
                        $data->update($formData);
                        Session::flash('success','Unit updated successfully.');
                    }else{  
                        Unit::create($formData);
                        Session::flash('success','Unit created successfully.');
                    }
                    Cache::forget('units');
                    return ['status' => 'true', 'message' => 'Records updated successfully.'];
                }
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } 
        }  
        return view('admin/units/add',compact('id','data','title','breadcrumbs'));
    }

    public function index(){ 
        $title = "Unit";
        $breadcrumbs = [ 
            ['name'=>'Unit','relation'=>'Current','url'=>'']
        ]; 
        return view('admin/units/index',compact('title','breadcrumbs'));
    }

    public function datatables(Request $request)
    {
        $pages = Unit::select(['id', 'name', 'status','position']); 
        return DataTables::of($pages) 
            ->addColumn('action', function ($page) {
                return '<a href="'.route('admin.units.add',$page->id).'" class="btn btn-xs btn-primary"><i class="fas fa-edit"></i> Edit</a>&nbsp;<a data-link="'.route('admin.units.delete').'" id="delete_'.$page->id.'" onclick="confirm_delete('.$page->id.')" href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fas fa-trash"></i> Delete</a>';
            })
            ->editColumn('status',function($page){
                return Helper::getStatus($page->status,$page->id,route('admin.units.status'));
            })  
            ->rawColumns(['status','action'])
            ->make(true);
    }

    public function status(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id; 
            $row = Unit::whereId($id)->first();
            $row->status = $row->status=='1'?'0':'1';
            $row->save(); 
            return Helper::getStatus($row->status,$id,route('admin.units.status'));
        }
    }  

    public function delete(Request $request)
    {
        if($request->ajax() && $request->isMethod('post')){
            $id = $request->id;
            try{
                $delete = Unit::where('id','=',$id)->delete();   
                if($delete){
                    return ["status"=>"true","message"=>"Record Deleted."]; 
                }else{
                    return ["status"=>"false","message"=>"Could not deleted Record."]; 
                } 
            }catch(\Exception $e){
                return ["status"=>"false","message"=>$e->getMessage()];   
            }
        }
    }
}
