<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache; 
use App\Models\User;  
use App\Models\StoreImage;  
use App\Models\StoreDetail;  
use App\Lib\Uploader;
use App\Lib\Email;
use App\Lib\Helper; 
use DB;
use Hash;
use Validator;
use Str;    
class StoreController extends Controller
{
    
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
           
    }  

    public function uploadStoreImage(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'image'          =>       'required|mimes:jpeg,bmp,png',    
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $store_id = $request->user()->id;
                $store_image = new StoreImage();
                $store_image->store_id = $store_id;
                if($request->file('image')!==null){
                    $destinationPath = "/uploads/stores/$store_id/";
                    $responseData = Uploader::doUpload($request->file('image'),$destinationPath,true,75);    
                    if($responseData['status']=="true"){ 
                        $store_image->image = $responseData['file']; 
                        $store_image->thumb = $responseData['thumb']; 
                        $store_image->save();
                    }                             
                }
                $images = StoreImage::where(['store_id'=>$store_id])->orderBy('id','desc')->get();
                return response()->json(['status' => 'true', 'message' => "Image uploaded",'data'=>$images]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function deleteStoreImage(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'id'          =>       'required|numeric',    
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $store_id = $request->user()->id;
                $image = StoreImage::where(['id'=>$request->id,'store_id'=>$store_id])->first();
                if($image){
                    if(file_exists($image->image)){
                        @unlink($image->image);
                    }
                    if(file_exists($image->thumb)){
                        @unlink($image->thumb);
                    }
                    $image->delete();
                }
                return response()->json(['status' => 'true', 'message' => "Image deleted!",'data'=>[]]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function updateStoreDetails(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'store_name'          =>       'required|max:150',    
                'address'             =>       'required|max:300',    
                'contact_number'      =>       'required|digits_between:7,15',    
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $store_id = $request->user()->id;
                StoreDetail::updateOrCreate(['store_id' => $store_id],[
                    'store_name'     => $request->store_name,
                    'address'        => $request->address,
                    'contact_number' => $request->contact_number,
                ]);
                return response()->json(['status' => 'true', 'message' => "Store details updated successfully",'data'=>[]]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getStoreDetails(Request $request){ 
        try {  
            $store_id = $request->user()->id;
            $store = StoreDetail::where(['store_id' => $store_id])->with(['images'])->first();
            return response()->json(['status' => 'true', 'message' => "Store details.",'data'=>$store]); 
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
 
}
