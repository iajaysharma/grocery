<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;
use App\Models\Country;
use App\Models\UserDevices;
use App\Models\User; 
use App\Models\Page; 
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Otp; 
use App\Models\AppSetting;
use App\Models\UserAddress;
use App\Models\StoreImage;
use App\Lib\Uploader;
use App\Lib\Email;
use App\Lib\Helper; 
use DB;
use Hash;
use Validator;
use Str;    
class UserController extends Controller
{
    
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
           
    } 

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */ 

    // This method use for signup
    protected function signup(Request $request){
        try {   
            $request->country_code = 91;
            $data = $request->all(); 
            $validator = Validator::make($data, [
                'role'              => 'required|in:USER,STORE', 
                'name'              => 'required|min:2|max:60|regex:/^[a-zA-Z\s]*$/', 
                'email'             => ['required','email',Rule::unique('users')->where(function($query) use($request){
                    $query->where(['role'=>$request->role,'deleted_at'=>NULL]);
                })], 
                'mobile'            =>  ['required','numeric','digits_between:7,15',Rule::unique('users')->where(function($query) use($request){
                    $query->where(['country_code'=>91,'deleted_at'=>NULL,'role'=>$request->role]);
                })],
                'password'          => 'required|min:8|max:45',
                'otp'               => 'nullable|numeric|digits_between:6,6',
                'device_type'       => 'required|in:IOS,ANDROID',
                'device_token'      => 'required', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator);
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $email = $request->email;
                if($request->filled('otp')){  
                    $check = Otp::where(['email'=>$email,'otp'=>$request->otp])->first();
                    if($check){  
                        DB::beginTransaction();
                        $check->delete();  
                        $password = Hash::make($request->get('password'));
                        $uuid = $this->generateUserUuid();  
                        $user = User::create([ 
                            'name'                  =>          trim($request->name), 
                            'email'                 =>          $request->email, 
                            'role'                  =>          $request->role, 
                            'country_code'          =>          $request->country_code,
                            'mobile'                =>          $request->mobile,
                            'password'              =>          $password,
                            'status'                =>          1,
                            'uuid'                  =>          $uuid     
                        ]);  
                        $user = User::getProfile($user->id); 
                        if($user){
                            UserDevices::deviceHandle([
                                "id"            =>  $user->id,
                                "device_type"   =>  $data['device_type'],
                                "device_token"  =>  $data['device_token'],
                            ]);
                        } 
                        $security_token = $user->createToken($request->device_type)->plainTextToken;  
                        DB::commit();
                        return response()->json(['status' => 'true', 'message' => "Logged In!",'data'=>$user,'security_token'=>$security_token]);  
                    }else{
                        $message = __("Invalid or incorrect OTP.");
                        return response()->json(['status' => 'false', 'message' => $message,'data'=>[]]);
                    }      
                }else{
                    // $otp = mt_rand(100000, 999999);  
                    $otp = 123456;  
                    $email_data['name'] = $request->name;
                    $email_data['otp'] = $otp;
                    Otp::where('email',$request->get('email'))->delete();
                    Otp::create(['email'=>$request->get('email'),'otp'=>$otp]);
                    $title = "${otp} is your one time password to proceed on ".env('APP_NAME');
                    Email::send('signup-otp',$email_data,$request->get('email'),$title);  
                    $data['otp'] = $otp; 
                    return response()->json(['status' => 'true', 'message' => __("Otp sent!"),'data'=>$data]);
                }  
            }  
        } catch (\Exception $e) { 
            DB::rollback();
            return response()->json(['status' => 'false', 'message' =>$e->getMessage(),'data'=>[]]); 
        }                     
    }

    // This method use for signin
    protected function signin(Request $request){
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'role'          => 'required|in:USER,STORE',
                'email'         => 'required',
                'password'      => 'required|min:8', 
                'device_type'   => 'required|in:IOS,ANDROID',
                'device_token'  => 'required',
            ]);
            if ($validator->fails()) {
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            } else { 
                if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) { 
                    $user = User::where(['email'=>$request->email,'role'=>$request->role])->first();
                } else {  
                    $user = User::where(['country_code'=>91,'mobile'=>$request->email,'role'=>$request->role])->first(); 
                }            
                if(!$user){ 
                    return response()->json(['status' => 'false', 'message' => "Invalid email/phone or password",'data'=>[]]);
                }else{
                    if($user->status==0){  
                        return response()->json(['status' => 'false', 'message' => __("Your account is inactive, please contact to administrator."),'data'=>[]]);
                    } 
                    if(Hash::check($data['password'],$user->password)){ 
                        UserDevices::deviceHandle([
                            "id"            =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]); 
                        $security_token = $user->createToken($request->device_type)->plainTextToken; 
                        return response()->json(['status' => 'true', 'message' => __("Logged In!"),'data'=>$user,'security_token'=>$security_token]); 
                    }else{
                        return response()->json(['status' => 'false', 'message' => "Invalid email/phone or password",'data'=>[]]);
                    } 
                }
            }
        }catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }  
    }

    protected function checkUser(Request $request){
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'role'              => 'required|in:USER,STORE',
                'name'              => 'required|min:2|max:60|regex:/^[a-zA-Z\s]*$/',
                'social_id'         => 'required', 
                'social_type'       => 'required|in:FACEBOOK,GOOGLE,APPLE', 
                'email'             => 'email',
                'device_type'       => 'required|in:IOS,ANDROID',
                'device_token'      => 'required',   
            ]);              
            if ($validator->fails()){
                $error = $this->validationHandle($validator);
                return response()->json(['status' => 'false', 'message' => $error]);  
            }else{ 
                if(isset($data['email']) && !empty($data['email'])){ 
                    $user = User::where('email','=',$data['email'])->where('role',$request->role)->first();
                }else{
                    $user = User::where('social_id','=',$data['social_id'])->where('role',$request->role)->first();
                }
                if($user){
                    if ($user->status==0){ 
                        return response()->json(['status' => 'false', 'message' => __("Your account is inactive, please contact to administrator."),'data'=>[]]);
                    }else{
                        UserDevices::deviceHandle([
                            "id"       =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]);
                        $security_token = $user->createToken($request->device_type)->plainTextToken;    
                        $user->is_new = false;
                        return response()->json(['status' => 'true', 'message' => __("Logged In!"),'data'=>$user,'security_token'=>$security_token]);
                    }
                }else{
                    $formData = [
                        'role'              =>  $data['role'],
                        'social_id'         =>  $data['social_id'],
                        'social_type'       =>  $data['social_type'],
                        'user_type'         =>  "Social",
                        'status'            =>  1  
                    ];
                    if($request->has('email') && $request->get('email') != ''){
                        $formData['email'] = $request->get('email');
                    }
                    if($request->has('name') && $request->get('name') != ''){
                        $formData['name'] = $request->get('name');
                    }
                    if($request->has('mobile') && $request->get('mobile') != ''){
                        $formData['mobile'] = $request->get('mobile');
                        $formData['country_code'] = 91;
                    } 
                    $user = User::create($formData);  
                    $user = User::find($user->id);
                    if($user){
                        UserDevices::deviceHandle([
                            "id"            =>  $user->id,
                            "device_type"   =>  $data['device_type'],
                            "device_token"  =>  $data['device_token'],
                        ]);
                    }   
                    $user->is_new = true;
                    $security_token = $user->createToken($request->device_type)->plainTextToken;  
                    return response()->json(['status' => 'true', 'message' => __("Logged In!"),'data'=>$user,'security_token'=>$security_token]);
                }
            }
        } catch (\Exception $e) { 
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }         
    }

    // This method use for get profile
    public function getProfile(Request $request){ 
        try{
            $userId = $request->user()->id; 
            $profile = User::getProfile($userId); 
            if($profile){ 
                return response()->json(['status' => 'true', 'message' => 'User Profile','data'=>$profile]);
            }else{
                return response()->json(['status' => 'false', 'message' => __("User Not Found"),'data'=>[]]);   
            } 
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]);         
        }  
    }

    // This method use for update profile
    public function updateProfile(Request $request){ 
        try {  
            $data = $request->all();
            $userId = $request->user()->id; 
            $validator = Validator::make($request->all(), [
                'name'              => 'required|min:2|max:60|regex:/^[a-zA-Z\s]*$/',    
                'profile_picture'   => 'nullable|mimes:jpeg,bmp,png,jpg', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]);  
            }else{  
                $user = User::find($userId);
                $user->name = trim($data['name']);    
                if($request->file('profile_picture')!==null){
                    $destinationPath = '/uploads/profile/';
                    $responseData = Uploader::doUpload($request->file('profile_picture'),$destinationPath,true,100);    
                    if($responseData['status']=="true"){ 
                        $user->profile_picture = $responseData['file']; 
                        $user->thumb = $responseData['thumb']; 
                    }                             
                }                          
                if($user->save()){           
                    return response()->json(['status' => 'true', 'message' => __("Profile updated successfully."),'data'=>$user]);
                }else{ 
                    return response()->json(['status' => 'false', 'message' => __("Unknown error accured while updating information."),'data'=>[]]);
                }
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]);         
        } 
    }

    // This method use for change password
    public function changePassword(Request $request){
        try {
            $data = $request->all();
            $validator = Validator::make($request->all(), [
                'current_password'          => 'required|min:8|max:45',
                'password'              => 'required|min:8|max:45|confirmed', 
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $userId = $request->user()->id; 
                $user = User::find($userId);
                if(!$user){
                    return response()->json(['status' => 'false', 'message' => __("User Not Found."),'data'=>[]]);
                }else{
                    if(Hash::check($data['password'],$user->password)){
                        $user->password = bcrypt($data['password']);
                        $user->save();
                        return response()->json(['status' => 'true', 'message' => __("Password change successfully."),'data'=>[]]);
                    }else{ 
                        return response()->json(['status' => 'false', 'message' => __("Current Password doesn't match."),'data'=>[]]);
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    // This method use for forgot password
    public function forgot(Request $request){
        try {
            $data = $request->all(); 
            $validator = Validator::make($data, [
                'role'          => 'required|in:USER,STORE',
                'email'         => 'required|email',
                'password'      => 'required_with:otp|min:8|max:45', 
            ]);
            if($validator->fails()){ 
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $user = User::where('email',$data['email'])->where('role',$request->role)->first();
                if(!$user){
                    return response()->json(['status' => 'false', 'message' => __("Email does not exist.")]); 
                }else{
                    if($user->status==0){ 
                        return response()->json(['status' => 'false', 'message' => __("Your account is inactive, please contact to administrator."),'data'=>[]]);
                    }else{
                        if($request->filled('otp')){
                            $check = Otp::where(['email'=>$request->email,'otp'=>$request->otp])->first();
                            if($check){  
                                $check->delete();
                                $password = Hash::make($request->get('password'));  
                                $user->update(['password'=>$password]); 
                                return response()->json(['status' => 'true', 'message' => __("Password changed!"),'data'=>[]]);  
                            }else{
                                $message = __("Invalid or incorrect OTP.");
                                return response()->json(['status' => 'false', 'message' => $message,'data'=>[]]);
                            }
                        }else{
                            // $otp = mt_rand(100000, 999999); 
                            $otp = 123456; 
                            $email_data['name'] = $user->name;
                            $email_data['otp'] = $otp;
                            Otp::where('email',$request->get('email'))->delete();
                            Otp::create(['email'=>$request->get('email'),'otp'=>$otp]);
                            $title = "${otp} is your ".env('APP_NAME')." recovery code";
                            Email::send('send-otp',$email_data,$request->get('email'),$title);  
                            $data['otp'] = $otp;  
                            return response()->json(['status' => 'true', 'message' => __("Otp Sent!"),'data'=>$data]); 
                        } 
                    }
                }
            }
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    // This method use for logout user.
    public function logout(Request $request){
        try {
            $validator = Validator::make($request->all(), [
                'device_type'       => 'required|in:ANDROID,IOS',
                'device_token'      => 'required',
            ]); 
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{    
                $userDevice = UserDevices::where(['device_type'=> $request['device_type'],'device_token'=>$request['device_token']])->first();
                if($userDevice){
                    $userDevice->delete();
                }
                $user = $request->user();
                $request->user()->currentAccessToken()->delete();
                return response()->json(['status' => 'true', 'message' => __("Logout Successfully.")]);
            }  
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }     
    }

    // list of country
    public function getCountry(){
        try{ 
            $countries = Cache::rememberForever('countries', function () {
                return Country::all();
            }); 
            return response()->json(['status' => 'true', 'message' => 'Country List Data','data'=>$countries]);
        }catch(\Exception $e){ 
            return response()->json(['status' => 'true', 'message' => $e->getMessage(),'data'=>[]]);
        }
    }

    public function setNotificationStatus(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'status'          =>       'required|in:0,1',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{
                $userId = $request->user()->id; 
                $status = $request->get('status');
                $user = User::find($userId);
                $user->update(['notification'=>$status]);
                if($user){
                    return response()->json(['status' => 'true', 'message' => __("Notification status updated successfully."),'data'=>$user]);
                }else{
                    return response()->json(['status' => 'false', 'message' => __("Error while updatig status, please try again."),'data'=>[]]);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function getStaticPage(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'slug'          =>       'required',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $slug = $request->get('slug');
                $page = Page::where('slug',$slug)->select('id','title','content')->first();
                if($page){
                    return response()->json(['status' => 'true', 'message' => 'Static page data.','data'=>$page]);
                }else{
                    return response()->json(['status' => 'false', 'message' => 'Invalid slug passed.','data'=>[]]);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function addAddress(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'id'                    =>       'nullable|numeric',   
                'address_type'          =>       'required|in:HOME,OFFICE,OTHER',
                'address'               =>       'required|max:300',
                'latitude'              =>       'required|max:50',
                'longitude'             =>       'required|max:50',
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $address = UserAddress::updateOrCreate(
                    [
                        'id'            =>          ($request->filled('id'))?$request->id:null,
                        'user_id'       =>          $request->user()->id
                    ],
                    [
                        'user_id'        =>         $request->user()->id,
                        'address_type'   =>         $request->address_type,    
                        'address'        =>         $request->address,    
                        'latitude'       =>         $request->latitude,    
                        'longitude'      =>         $request->longitude,    
                    ]
                ); 
                return response()->json(['status' => 'true', 'message' =>($request->filled('id'))?"Address updated successfully.":"Address added successfully.",'data'=>$address]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    public function deleteUserAddress(Request $request){ 
        try { 
            $validator = Validator::make($request->all(), [
                'id'                    =>       'nullable|numeric',    
            ]);
            if ($validator->fails()){
                $error = $this->validationHandle($validator); 
                return response()->json(['status' => 'false', 'message' => $error]); 
            }else{ 
                $address =  UserAddress::where(['id'=>$request->id,'user_id' => $request->user()->id])->firstorfail();
                $address->delete();
                return response()->json(['status' => 'true', 'message' =>"Address deleted successfully",'data'=>[]]);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
  

    public function getUserAddresses(Request $request){ 
        try {               
            $userId = $request->user()->id;    
            $addresses = UserAddress::where('user_id',$userId)->orderBy('id','desc')->get(); 
            return response()->json(['status' => 'true', 'message' => "User Addresses",'data'=>$addresses]);    
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }  

    public function getSettings(Request $request){ 
        try { 
            $settings = Setting::all()->pluck('value','field_name')->toArray();
            return response()->json(['status' => 'true', 'message' => 'Data.','data'=>$settings]);
              
        } catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }
   

    public function getAppSetting(Request $request){
        try {  
            $app_settings = Cache::rememberForever('app_settings', function (){
                return  AppSetting::where('id','1')->first();
            }); 
            return response()->json(['status'=>'true','message'=>__('App Setting'),'data'=>$app_settings]); 
        }catch (\Exception $e) {
            return response()->json(['status' => 'false', 'message' => $e->getMessage(),'data'=>[]]); 
        }
    }

    

    
 
   
}
