<?php 
use App\Models\Setting;  
use App\Models\Category; 
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
/*function settingCache($key=null){
    if($key){
        if(session()->get('SiteValue.'.$key)){   
            return session()->get('SiteValue.'.$key);
        }else{  
            $setting = Setting::pluck('value','field_name')->toArray(); 
            foreach($setting as $kkey => $val){
                session()->put('SiteValue.'.$kkey, $val);
            }
            if(isset($setting[$key])){
                return $setting[$key];
            }else {
                return '';
            } 
        }
    }else{
        return '';
    }
}*/

function settingCache($key=null){
    if($key){
        $value = Cache::rememberForever($key, function () use ($key) {
            $check = Setting::where('field_name',$key)->first();
            if($check){
                return $check->value;
            }else{
                return '';
            }
        }); 
        return $value;
    }else{
        return '';
    }
}



function setting($key=null){
    if($key){
        $setting = Setting::where('field_name',$key)->first(); 
        if($setting){
            return $setting->value;
        }else{
            return '';
        } 
    }else{
        return '';
    }
}

function getFileType($file){
    $mime = $file->getMimeType();  
    if(strstr($mime, "video/")){
        $filetype = "video";
    }else if(strstr($mime, "image/")){
        $filetype = "image";
    }else if(strstr($mime, "audio/")){
        $filetype = "audio";
    }else if(strstr($mime, "pdf/")){
        $filetype = "pdf";
    }else if(strstr($mime, "application/")){
        $ext = $file->getClientOriginalExtension();
        $allowed = array('xml','csv','doc','docx','xls','xlsx','ppt','pptx','txt','key','odp','pps','ods','xlsm','odt','rtf','tex','wpd');
        if(in_array( $ext, $allowed ) ){
            $filetype = 'document';
        }else{
            $filetype = ''; 
        }
    }else{
        $filetype = '';
    }  
    return $filetype;
}

function generateImage($name,$size=50){
    return "https://ui-avatars.com/api?rounded=true&size=${size}&background=random&name=${name}";
}

function getStripeFee($amount){
    $amt = (float)round(($amount*2.9/100)+0.30,2);
    return $amt;
}

function num_format($number,$precision=2){
    $amt = number_format((float)$number, 2, '.', '');
    return (float)$amt;
    // return floor($number) . substr(str_replace(floor($number), '', $number), 0, $precision + 1);
}

function showCardImage($type){
    switch ($type) {
        case "mastercard":
            return '<img src="https://img.icons8.com/fluent/48/000000/mastercard.png"/>';
            break;
        case "visa":
            return '<img src="https://img.icons8.com/color/48/000000/visa.png"/>';
            break;
        case "discover":
            return '<img src="https://img.icons8.com/color/48/000000/discover.png"/>';
            break;
        case "jcb":
            return '<img src="https://img.icons8.com/color/48/000000/jcb.png"/>';
            break;
        case "amex":
            return '<img src="https://img.icons8.com/color/48/000000/amex.png"/>';
            break;
        case "unionpay":
            return '<img src="https://img.icons8.com/fluent/48/000000/unionpay.png"/>';
            break;    
        
        default:
            return ucfirst($type);
            break;
    }
}

function getCpaSpecializationCategories($id,$group=false,$limit=null){
    try {
        if($group){
            $cats = CpaSpecializationCategory::join('efillings','cpa_specialization_categories.efilling_id','=','efillings.id')->where(['cpa_specialization_categories.user_id'=>$id,'efillings.status'=>1,'efillings.deleted_at'=>null])->pluck('efillings.parent_id','efillings.id')->toArray();  
            if(count($cats)){
                $parent_cats = array_unique(array_values($cats));
                $child_cats = array_keys($cats);
                $efillings = Efilling::whereIn('id',$parent_cats)->select('id','title')->get()->toArray();
                foreach ($efillings as $key => $value) {
                    $efillings[$key]['childrens'] = Efilling::where(['parent_id'=>$value['id'],'status'=>1])->whereIn('id',$child_cats)->select('id','title')->get()->toArray();
                }
                return $efillings;
            }else{
                return [];
            }
        }else{
            $cats = CpaSpecializationCategory::join('efillings','cpa_specialization_categories.efilling_id','=','efillings.id')->where(['cpa_specialization_categories.user_id'=>$id,'efillings.status'=>1,'efillings.deleted_at'=>null])->select('efillings.id','efillings.title');
            if($limit){
                $cats = $cats->limit($limit);
            }
            $cats = $cats->get()->toArray();
            return $cats;
        }   
    } catch (\Exception $e) {
        return [];
    }
}

function makeSlug($str){
    return Str::slug($str);
}

function buildTree(array $elements, $parentId = null) {
    $branch = array();  
    
    foreach ($elements as $element) {
        if ($element['parent_id'] == $parentId) {
            $children = buildTree($elements, $element['id']);
            
            if ($children) {
                $element['children'] = $children;
            }
            
            $branch[] = $element;
        }
    }
    
    return $branch;
}

function categoryTree($selected=null,$parent_id = null, $sub_mark = ''){ 
    $categories = Category::where('parent_id',$parent_id)->orderBy('title','ASC')->get()->toArray();  
    foreach($categories as $key => $category){ 
        $sel = ($category['id']==$selected)?"selected":"";
        echo '<option '.$sel.' value="'.$category['id'].'">'.$sub_mark.$category['title'].'</option>';  
        categoryTree($selected,$category['id'], $sub_mark.'---'); 
    }    
}
 